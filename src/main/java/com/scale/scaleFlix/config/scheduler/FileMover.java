package com.scale.scaleFlix.config.scheduler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.ItemWriteListener;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;

@Component
@Slf4j
public class FileMover implements ItemWriteListener<List> {


    @Override
    public void beforeWrite(List list) {
        System.out.println("BEFORE WRITE");
    }

    @Override
    public void afterWrite(List list) {
        fileMover();
    }

    @Override
    public void onWriteError(Exception e, List list) {
        fileMover();
    }

    private void fileMover() {
        String fromFile = "C:\\PeyoPC\\Projects\\HWProjects\\HW\\ScaleFlixCatalog\\scaleflix\\input\\user.csv";
        String toFile = "C:\\PeyoPC\\Projects\\HWProjects\\HW\\ScaleFlixCatalog\\scaleflix\\input\\archive";

        Path source = Paths.get(fromFile);
        Path target = Paths.get(toFile);

        try {

            // rename or move a file to other path
            // if target exists, throws FileAlreadyExistsException
            //    Files.move(source, target);

            // if target exists, replace it.
            Files.move(source, target.resolve(source.getFileName()), StandardCopyOption.REPLACE_EXISTING);

            // multiple CopyOption
//            CopyOption[] options = {StandardCopyOption.REPLACE_EXISTING,
//                    StandardCopyOption.COPY_ATTRIBUTES,
//                    LinkOption.NOFOLLOW_LINKS};
//
//            Files.move(source, target, options);

        } catch (IOException e) {
//            System.out.println((String.format("Error processing file from %s",e.getStackTrace())));
            e.getStackTrace();
        }
    }
}