package com.scale.scaleFlix.config.scheduler;

import com.scale.scaleFlix.domain.User;
import org.springframework.batch.core.ItemWriteListener;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.repository.support.JobRepositoryFactoryBean;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.batch.support.DatabaseType;
import org.springframework.batch.support.transaction.ResourcelessTransactionManager;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.core.io.FileSystemResource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.init.DatabasePopulatorUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;

import javax.sql.DataSource;

@Configuration
@EnableBatchProcessing
public class SpringBatchConfig {
    @Bean
    @Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
    public User user() {
        return new User();
    }

    @Bean
    @Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
    public ItemProcessor<User, User> itemProcessor() {
        return new UserItemProcessor();
    }

    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3306/scaleflix");
        dataSource.setUsername("root");
        dataSource.setPassword("1234");
        ResourceDatabasePopulator databasePopulator = new ResourceDatabasePopulator();
//        databasePopulator.addScript(new ClassPathResource("org/springframework/batch/core/schema-drop-mysql.sql"));
//        databasePopulator.addScript(new ClassPathResource("org/springframework/batch/core/schema-mysql.sql"));
        DatabasePopulatorUtils.execute(databasePopulator, dataSource);
        return dataSource;
    }

    @Bean
    public ResourcelessTransactionManager txManager() {
        return new ResourcelessTransactionManager();
    }

    @Bean
    public JobRepository jbRepository(DataSource dataSource, ResourcelessTransactionManager transactionManager)
            throws Exception {
        JobRepositoryFactoryBean factory = new JobRepositoryFactoryBean();
        factory.setDatabaseType(DatabaseType.MYSQL.getProductName());
        factory.setDataSource(dataSource);
        factory.setTransactionManager(transactionManager);
        return factory.getObject();
    }

    @Bean
    public JobLauncher jbLauncher(JobRepository jobRepository) {
        SimpleJobLauncher jobLauncher = new SimpleJobLauncher();
        jobLauncher.setJobRepository(jobRepository);
        return jobLauncher;
    }

    @Bean
    public BeanWrapperFieldSetMapper<User> beanWrapperFieldSetMapper() {
        BeanWrapperFieldSetMapper<User> fieldSetMapper = new BeanWrapperFieldSetMapper<>();
        fieldSetMapper.setPrototypeBeanName("user");
        return fieldSetMapper;
    }

    @Bean
    public FlatFileItemReader<User> fileItemReader(BeanWrapperFieldSetMapper<User> beanWrapperFieldSetMapper) {
        FlatFileItemReader<User> fileItemReader = new FlatFileItemReader<>();
        fileItemReader.setResource(new FileSystemResource("input/user.csv"));
        DelimitedLineTokenizer delimitedLineTokenizer = new DelimitedLineTokenizer();
        delimitedLineTokenizer.setNames("id", "username", "email", "password");
        DefaultLineMapper<User> defaultLineMapper = new DefaultLineMapper<>();
        defaultLineMapper.setLineTokenizer(delimitedLineTokenizer);
        defaultLineMapper.setFieldSetMapper(beanWrapperFieldSetMapper);
        fileItemReader.setLineMapper(defaultLineMapper);
        return fileItemReader;
    }

    @Bean
    public JdbcBatchItemWriter<User> jdbcBatchItemWriter(DataSource dataSource,
                                                         BeanPropertyItemSqlParameterSourceProvider<User> sqlParameterSourceProvider) {
        JdbcBatchItemWriter<User> jdbcBatchItemWriter = new JdbcBatchItemWriter<>();
        jdbcBatchItemWriter.setDataSource(dataSource);
        jdbcBatchItemWriter.setItemSqlParameterSourceProvider(sqlParameterSourceProvider);
        jdbcBatchItemWriter.setSql("insert into users(user_id,username,email,password) values (:id, :username, :email, :password)");
        return jdbcBatchItemWriter;
    }

    @Bean
    public BeanPropertyItemSqlParameterSourceProvider<User> beanPropertyItemSqlParameterSourceProvider() {
        return new BeanPropertyItemSqlParameterSourceProvider<>();
    }

    @Bean
    public Job jobCsvMysql(JobBuilderFactory jobBuilderFactory, Step step) {
        return jobBuilderFactory.get("jobCsvMysql").incrementer(new RunIdIncrementer()).flow(step).end().build();
    }

    @Bean
    public Step step1(StepBuilderFactory stepBuilderFactory, ResourcelessTransactionManager transactionManager,
                      ItemReader<User> reader, ItemWriter<User> writer, ItemProcessor<User, User> processor, ItemWriteListener listener) {

        return stepBuilderFactory.get("step1").transactionManager(transactionManager).<User, User>chunk(8)
                .reader(reader).processor(processor).writer(writer).listener(listener).build();

    }

//    @Bean
//    public Step step2(StepBuilderFactory stepBuilderFactory, ResourcelessTransactionManager transactionManager,
//                      ItemReader<User> reader, ItemWriter<User> writer, ItemProcessor<User, User> processor) {
//        return stepBuilderFactory.get("step2").transactionManager(transactionManager).<User, User>chunk(2)
//                .reader(reader).processor(processor).writer(writer).build();
//    }


}