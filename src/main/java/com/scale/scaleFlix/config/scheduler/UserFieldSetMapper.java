package com.scale.scaleFlix.config.scheduler;

import com.scale.scaleFlix.domain.User;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;

public class UserFieldSetMapper implements FieldSetMapper<User> {

    @Override
    public User mapFieldSet(FieldSet fieldSet) {
        User user = new User();
        user.setId(fieldSet.readLong(0));
        user.setUsername(fieldSet.readString(1));
        user.setEmail(fieldSet.readString(2));
        user.setPassword(fieldSet.readString(3));
        return user;
    }
}