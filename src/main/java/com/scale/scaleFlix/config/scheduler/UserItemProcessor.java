package com.scale.scaleFlix.config.scheduler;

import com.scale.scaleFlix.domain.User;
import org.springframework.batch.item.ItemProcessor;

public class UserItemProcessor implements ItemProcessor<User, User> {

    @Override
    public User process(User user) throws Exception {
        System.out.println("Processing: " + user);
        final String initCapUserName = user.getUsername().substring(0, 1).toUpperCase()
                + user.getUsername().substring(1);
        final String initCapEmail = user.getEmail().substring(0, 1).toUpperCase()
                + user.getEmail().substring(1);
        final String initCapPassword = user.getPassword().substring(0, 1).toUpperCase()
                + user.getPassword().substring(1);
        User transformedUser = new User();
        transformedUser.setId(user.getId());
        transformedUser.setUsername(initCapUserName);
        transformedUser.setEmail(initCapEmail);
        transformedUser.setPassword(initCapPassword);
//        transformedUser.setRegularUser(true);
        return transformedUser;
    }

}
