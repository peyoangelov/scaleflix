package com.scale.scaleFlix.controllers;

import com.scale.scaleFlix.dto.movie.MovieRequestDTO;
import com.scale.scaleFlix.dto.movie.MovieResponseDTO;
import com.scale.scaleFlix.services.MovieService;
import io.swagger.annotations.*;
import lombok.AllArgsConstructor;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping(value = "/movies")
@Api(tags = "Movies")
public class MovieController {

    private final MovieService movieService;

    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER', 'USER')")
    @GetMapping
    @ApiOperation(nickname = "getAllMovies", value = "Get all movies", notes = "return all movies")
    @ApiResponse(code = 200, message = "All movies returned")
    public List<MovieResponseDTO> getAllMovies() {
        return movieService.getAllMovies();
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER', 'USER')")
    @GetMapping(value = "/{id}")
    @ApiOperation(nickname = "getMovie", value = "Get movie by Id", notes = "return movie")
    @ApiResponses(
            {
                    @ApiResponse(code = 200, message = "Returns movie by Id"),
                    @ApiResponse(code = 404, message = "Movie not found")
            })
    public ResponseEntity<MovieResponseDTO> getMovie(@ApiParam(value = "Movie Id", required = true, example = "1") @PathVariable long id) {
        try {
            return new ResponseEntity<>(movieService.getMovieById(id), HttpStatus.OK);
        } catch (ChangeSetPersister.NotFoundException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER', 'USER')")
    @GetMapping(value = "/movie-title/{name}")
    @ApiOperation(nickname = "findMovieByTitle", value = "Get movie by title", notes = "return movie")
    @ApiResponses(
            {
                    @ApiResponse(code = 200, message = "Returns movie by title"),
                    @ApiResponse(code = 404, message = "Movie not found")
            })
    public ResponseEntity<MovieResponseDTO> findMovieByTitle(@ApiParam(value = "Movie title", required = true, example = "X man") @PathVariable String name) {
        try {
            return new ResponseEntity<>(movieService.getMovieByTitle(name), HttpStatus.OK);
        } catch (ChangeSetPersister.NotFoundException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER')")
    @PostMapping(value = "/add-movie")
    @ApiOperation(nickname = "addMovieToTheSystem", value = "Add a movie")
    @ApiResponses(
            {
                    @ApiResponse(code = 200, message = "Movie is imported successfully"),
                    @ApiResponse(code = 404, message = "Adding movie Error")
            })
    public ResponseEntity<String> addMovieToTheSystem(@Valid @ModelAttribute MovieRequestDTO movieRequestDTO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return new ResponseEntity<>("Adding movie Error", HttpStatus.CONFLICT);
        }
        try {
            movieService.importMovieToDb(movieRequestDTO);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
        return new ResponseEntity<>("Movie is imported successfully", HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER')")
    @DeleteMapping("/delete/{id}/")
    @ApiOperation(nickname = "deleteMovie", value = "Delete movie by id", notes = "delete a movie")
    @ApiResponse(code = 200, message = "movie is deleted successfully")
    public ResponseEntity<String> deleteMovie(@ApiParam(value = "Movie Id", required = true, example = "1") @PathVariable String id) {
        movieService.deleteMovieById((Long.valueOf(id)));

        return new ResponseEntity<>("Movie is deleted successfully", HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER')")
    @DeleteMapping("/delete-movie/{title}/")
    @ApiOperation(nickname = "deleteMovie", value = "Delete movie by title", notes = "delete a movie")
    @ApiResponse(code = 200, message = "movie is deleted successfully")
    public ResponseEntity<String> deleteMovieByTitle(@ApiParam(value = "Movie title", required = true, example = "Xman") @PathVariable String title) {
        movieService.deleteMovieByTitle(title);

        return new ResponseEntity<>("Movie is deleted successfully", HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER', 'USER')")
    @GetMapping(value = "/movies-genre/{genre}")
    @ApiOperation(nickname = "getAllMoviesWithGenre", value = "Get movies by genre", notes = "return movies")
    @ApiResponse(code = 200, message = "All movies with specific genre returned")
    public List<MovieResponseDTO> getAllMoviesWithGenre(@ApiParam(value = "genre", required = true, example = "action") @PathVariable String genre) {

        return movieService.getMoviesByGenre(genre);
    }


}
