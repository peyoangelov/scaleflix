package com.scale.scaleFlix.controllers;

import com.scale.scaleFlix.dto.serial.SerialRequestDTO;
import com.scale.scaleFlix.dto.serial.SerialResponseDTO;
import com.scale.scaleFlix.services.SerialService;
import io.swagger.annotations.*;
import lombok.AllArgsConstructor;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping(value = "/serials")
@Api(tags = "Serials")
public class SerialController {

    private final SerialService serialService;

    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER', 'USER')")
    @GetMapping
    @ApiOperation(nickname = "getAllSerials", value = "Get All Serials", notes = "returns all serials")
    @ApiResponse(code = 200, message = "All series returned")
    public List<SerialResponseDTO> getAllSeries() {
        return serialService.getAllSeries();
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER', 'USER')")
    @GetMapping(value = "/{id}")
    @ApiOperation(nickname = "getSerial", value = "Get serial by Id", notes = "return serial")
    @ApiResponses(
            {
                    @ApiResponse(code = 200, message = "Returns serial by Id"),
                    @ApiResponse(code = 404, message = "Serial not found")
            })
    public ResponseEntity<SerialResponseDTO> getSerial(@ApiParam(value = "Serial Id", required = true, example = "1") @PathVariable long id) {
        try {
            return new ResponseEntity<>(serialService.getSerialById(id), HttpStatus.OK);
        } catch (ChangeSetPersister.NotFoundException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER', 'USER')")
    @GetMapping(value = "/serial-title/{name}")
    @ApiOperation(nickname = "findSerialByTitle", value = "Get serial by name", notes = "return serial")
    @ApiResponses(
            {
                    @ApiResponse(code = 200, message = "Returns serial by name"),
                    @ApiResponse(code = 404, message = "Serial not found")
            })
    public ResponseEntity<SerialResponseDTO> findSerialByName(@ApiParam(value = "Serial name", required = true, example = "Lost") @PathVariable String name) {
        try {
            return new ResponseEntity<>(serialService.getSerialByTitle(name), HttpStatus.OK);
        } catch (ChangeSetPersister.NotFoundException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER')")
    @PostMapping(value = "/add-serial")
    @ApiOperation(nickname = "addSerial", value = "Add a serial")
    @ApiResponses(
            {
                    @ApiResponse(code = 200, message = "Serial is imported successfully"),
                    @ApiResponse(code = 404, message = "Adding serial Error")
            })
    public ResponseEntity<String> addSerialToTheSystem(@Valid @ModelAttribute SerialRequestDTO serialRequestDTO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return new ResponseEntity<>("Adding serial Error", HttpStatus.CONFLICT);
        }
        try {
            serialService.importSerialToDb(serialRequestDTO);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
        return new ResponseEntity<>("Serial is imported successfully", HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER')")
    @DeleteMapping("/delete/{id}")
    @ApiOperation(nickname = "deleteSerial", value = "Delete serial by id", notes = "delete a serial")
    @ApiResponse(code = 200, message = "Serial is deleted successfully")
    public ResponseEntity<String> deleteSerial(@ApiParam(value = "Serial Id", required = true, example = "1") @PathVariable String id) {
        serialService.deleteSerialById(Long.valueOf(id));

        return new ResponseEntity<>("Serial is deleted successfully", HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER', 'USER')")
    @GetMapping(value = "/serials-genre/{genre}")
    @ApiOperation(nickname = "getAllSerialsWithGenre", value = "Get serials by genre", notes = "return serials")
    @ApiResponse(code = 200, message = "All serials with specific genre  returned")
    public List<SerialResponseDTO> getAllSerialsWithGenre(@ApiParam(value = "genre", required = true, example = "action") @PathVariable String genre) {

        return serialService.getSerialsByGenre(genre);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER')")
    @DeleteMapping("/delete-serial/{title}/")
    @ApiOperation(nickname = "deleteSerial", value = "Delete serial by title", notes = "delete a movie")
    @ApiResponse(code = 200, message = "serial is deleted successfully")
    public ResponseEntity<String> deleteSerialByTitle(@ApiParam(value = "Serial title", required = true, example = "Xman") @PathVariable String title) {
        serialService.deleteSerialByTitle(title);

        return new ResponseEntity<>("Movie is deleted successfully", HttpStatus.OK);
    }
}
