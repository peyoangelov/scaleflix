package com.scale.scaleFlix.controllers;

import com.scale.scaleFlix.dto.movie.MovieResponseDTO;
import com.scale.scaleFlix.dto.serial.SerialResponseDTO;
import com.scale.scaleFlix.dto.user.UserRequestDTO;
import com.scale.scaleFlix.dto.user.UserResponseDTO;
import com.scale.scaleFlix.dto.user.UserRoleUpdateDTO;
import com.scale.scaleFlix.dto.user.UserUpdateDTO;
import com.scale.scaleFlix.principal.UserPrincipal;
import com.scale.scaleFlix.services.MovieService;
import com.scale.scaleFlix.services.SerialService;
import com.scale.scaleFlix.services.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@AllArgsConstructor
@Api(tags = "Users")
@RequestMapping("/users")
public class UserController {

    private final UserService userService;
    private final MovieService movieService;
    private final SerialService serialService;

    @PostMapping(value = "/register", consumes = {MediaType.APPLICATION_JSON_VALUE})
    @ApiOperation("Register new user")
    public ResponseEntity<?> registerUser(@Valid @RequestBody UserRequestDTO requestDTO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return new ResponseEntity<>(bindingResult.getAllErrors().stream().map(ObjectError::getDefaultMessage).collect(Collectors.toList()), HttpStatus.BAD_REQUEST);
        }
        userService.save(requestDTO);
        return new ResponseEntity<>("The User is registered successfully!", HttpStatus.CREATED);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @GetMapping
    @ApiOperation("Get all users")
    public ResponseEntity<List<UserResponseDTO>> getAllUsers() {
        List<UserResponseDTO> users = userService.findAll();
        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @GetMapping("/{id}")
    @ApiOperation("Get user by UserId")
    public ResponseEntity<UserResponseDTO> getUserById(@PathVariable Long id) {
        UserResponseDTO user = userService.getUserById(id);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @GetMapping("/email/{email}")
    @ApiOperation("Get user by Email")
    public ResponseEntity<UserResponseDTO> getUserByEmail(@PathVariable String email) {

        UserResponseDTO user = userService.getUserByEmail(email);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

//    @PreAuthorize("hasAuthority('ADMIN')")
//    @GetMapping("/username/{username}")
//    @ApiOperation("Get user by username")
//    public ResponseEntity<UserResponseDTO> getUserByUsername(@PathVariable String username) {
//        UserResponseDTO user = userService.getUserByUsername(username);
//        return new ResponseEntity<>(user, HttpStatus.OK);
//    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @DeleteMapping("/delete-user/{id}")
    @ApiOperation("Delete an user")
    public ResponseEntity<String> deleteUser(@PathVariable Long id) {

        String response = userService.deleteUserById(id);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @PutMapping("/{id}")
    @ApiOperation("Update specific user information")
    public ResponseEntity<UserResponseDTO> updateUser(@Valid @RequestBody UserUpdateDTO updateDTO,
                                                      @PathVariable Long id) {
        return new ResponseEntity<>(userService.update(id, updateDTO), HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @PatchMapping("/update-user-role/{id}")
    @ApiOperation("Update user role")
    public ResponseEntity<UserResponseDTO> updateUserRole(@Valid @RequestBody UserRoleUpdateDTO userRoleUpdateDTO,
                                                          @PathVariable Long id) {
        return new ResponseEntity<>(userService.updateUserRole(id, userRoleUpdateDTO), HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER', 'USER')")
    @PatchMapping("/{username}/movie/{movieId}")
    @ApiOperation("Add movie to favor user's list")
    public ResponseEntity<String> addMovieToFavoriteList(@AuthenticationPrincipal UserPrincipal principal,
                                                         @PathVariable String username,
                                                         @PathVariable Long movieId) {

        Optional<UserResponseDTO> optionalUserResponseDTO = userService.findUserInfoByUsername(principal, username)
                .stream()
                .findFirst();
        Long userId = optionalUserResponseDTO.get().getId();
        userService.addMovieToFavoriteList(userId, movieId);

        String response = "Movie has been added successfully to the user favorite list";
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER', 'USER')")
    @PatchMapping("/{username}/serial/{serialId}")
    @ApiOperation("Add serial to favor user's list")
    public ResponseEntity<String> addSerialToFavoriteList(@AuthenticationPrincipal UserPrincipal principal,
                                                          @PathVariable String username,
                                                          @PathVariable Long serialId) {
        Optional<UserResponseDTO> optionalUserResponseDTO = userService.findUserInfoByUsername(principal, username)
                .stream()
                .findFirst();
        Long userId = optionalUserResponseDTO.get().getId();
        userService.addSerialToFavoriteList(userId, serialId);

        String response = "Serial has been added successfully to the user favorite list";
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER', 'USER')")
    @GetMapping("/name/{username}")
    @ApiOperation("Get specific user info by their usernames")
    public ResponseEntity<List<UserResponseDTO>> getUserInfoByUserName(@AuthenticationPrincipal UserPrincipal principal,
                                                                       @PathVariable String username) {
        List<UserResponseDTO> user = userService.findUserInfoByUsername(principal, username);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER', 'USER')")
    @GetMapping("/{username}/movies")
    @ApiOperation("Get user's favorite list of movies")
    public ResponseEntity<List<MovieResponseDTO>> getUserFavoritesMoviesByUserName(@AuthenticationPrincipal UserPrincipal principal,
                                                                                   @PathVariable String username) {
        Optional<UserResponseDTO> optionalUserResponseDTO = userService.findUserInfoByUsername(principal, username).stream().findFirst();
        Long userId = optionalUserResponseDTO.get().getId();
        List<MovieResponseDTO> movies = movieService.getUserFavoritesMovies(userId);
        return new ResponseEntity<>(movies, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER', 'USER')")
    @GetMapping("/{username}/serials")
    @ApiOperation("Get user's favorite list of serials")
    public ResponseEntity<List<SerialResponseDTO>> getUserFavoritesSerialsByUserName(@AuthenticationPrincipal UserPrincipal principal,
                                                                                     @PathVariable String username) {
        Optional<UserResponseDTO> optionalUserResponseDTO = userService.findUserInfoByUsername(principal, username).stream().findFirst();
        Long userId = optionalUserResponseDTO.get().getId();
        List<SerialResponseDTO> serials = serialService.getUserFavoritesSerials(userId);
        return new ResponseEntity<>(serials, HttpStatus.OK);
    }
}
