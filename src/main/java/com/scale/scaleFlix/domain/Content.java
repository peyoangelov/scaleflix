package com.scale.scaleFlix.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@Table(name = "content",
        uniqueConstraints =
        @UniqueConstraint(columnNames = {"title", "imdbId"}))
@MappedSuperclass
public class Content {
    @Column(length = 100)
    String title;
    @Column(length = 3000)
    String description;
    @Column
    double rating;
    @Column
    Date releaseDate;
    @Column(length = 100)
    String director;
    @Column(length = 100)
    String writer;
    @Column
    double stars;
    @Column
    double duration;
    @Column()
    String imdbId;
    @Column
    String year;
    @Column
    String genre;
    @Column
    String audio;
    @Column
    String subtitles;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

}
