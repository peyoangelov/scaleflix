package com.scale.scaleFlix.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "serials")
public class Serial extends Content {
    @JsonIgnore
    @ManyToMany(mappedBy = "serials")
    private List<User> users;
    @Column
    Short episodeNumber;
    @Column
    Short seasonNumber;
}
