package com.scale.scaleFlix.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "users")
@NoArgsConstructor
public class User  {
//    implements UserDetails
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
@Column(name = "user_id")
private Long id;
    @Column(length = 100,name = "username", nullable = false, unique = true)
    private String username;
    @Column(name = "password", nullable = false)
    private String password;
    @Email
    @Column(name = "email", nullable = false,unique = true)
    private String email;
    @Column(name = "is_manager")
    private boolean isManager;

    @Column(name = "is_administrator")
    private boolean isAdmin;

    @Column(name = "is_regular_user")
    private boolean isRegularUser;

    @ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(
            name = "user_serials",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "serials_id")}
    )
    private Set<Serial> serials;

    @ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(
            name = "user_movie",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "movie_id")}
    )
    private Set<Movie> movies;
//
//    @Column
//    private boolean isAccountNonExpired;
//
//    @Column
//    private boolean isAccountNonLocked;
//
//    @Column
//    private boolean isCredentialsNonExpired;
//
//    @Column
//    private boolean isEnabled;
//
//    @ManyToMany(fetch = FetchType.EAGER)
//    private Set<Role> authorities;
//
//    public User() {
//        authorities = new HashSet<>();
//    }


    public User(String username, String password, String email) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.isManager = false;
        this.isAdmin = false;
        this.isRegularUser = true;
        this.serials = new HashSet<>();
        this.movies = new HashSet<>();
    }
}
