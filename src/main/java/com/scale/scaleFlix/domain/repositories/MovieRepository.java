package com.scale.scaleFlix.domain.repositories;

import com.scale.scaleFlix.domain.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface MovieRepository extends JpaRepository<Movie, Long> {

    Movie findByTitle(String name);

    List<Movie> findByGenre(String name);

    boolean existsByImdbId(String id);

    void deleteById(Long id);

    void  deleteByTitle(String title);
}
