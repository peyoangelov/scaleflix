package com.scale.scaleFlix.domain.repositories;

import com.scale.scaleFlix.domain.Serial;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface SerialRepository extends JpaRepository<Serial, Long> {
    Serial findByTitle(String name);

    List<Serial> findByGenre(String name);

    boolean existsByImdbId(String id);

    void deleteById(Long id);

    void deleteByTitle(String title);
}
