package com.scale.scaleFlix.domain.repositories;

import com.scale.scaleFlix.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
@Transactional
public interface UserRepository extends JpaRepository<User, Long> {
    boolean existsByUsername(String username);

    Optional<User> findByUsernameAndPassword(String username, String password);

    Optional<User> findByUsername(String username);

    User getById(Long aLong);

    void deleteById(Long id);

    Optional<User> findByEmail(String email);


//    @Query("select u from User u inner join u.authorities r where r.authority in :role")
//    List<User> getUserList(@Param("role") String userRole);
}
