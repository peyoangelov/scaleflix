package com.scale.scaleFlix.dto.mapper;

import com.scale.scaleFlix.domain.Movie;
import com.scale.scaleFlix.dto.movie.MovieRequestDTO;
import com.scale.scaleFlix.dto.movie.MovieResponseDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface MovieMapper {

    MovieMapper INSTANCE = Mappers.getMapper(MovieMapper.class);

    MovieResponseDTO movieToMovieResponseDTO(Movie movie);

    Movie movieRequestDTOToMovie(MovieRequestDTO movieRequestDTO);

}
