package com.scale.scaleFlix.dto.mapper;

import com.scale.scaleFlix.domain.Serial;
import com.scale.scaleFlix.dto.serial.SerialRequestDTO;
import com.scale.scaleFlix.dto.serial.SerialResponseDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface SerialMapper {

    SerialMapper INSTANCE = Mappers.getMapper(SerialMapper.class);

    SerialResponseDTO serialToSerialResponseDTO(Serial serial);

    Serial serialRequestDTOToSerial(SerialRequestDTO serialRequestDTO);

}
