package com.scale.scaleFlix.dto.mapper;

import com.scale.scaleFlix.domain.User;
import com.scale.scaleFlix.dto.user.UserRequestDTO;
import com.scale.scaleFlix.dto.user.UserResponseDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface UserMapper {

    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);


    UserResponseDTO userToUserRespDTO(User user);

    User userReqDTOToUser(UserRequestDTO userRequestDTO);
//
//  @Mapping(source = "firstName", target = "firstName")
//  EmployeeNameResponseDTO empToEmpNameDTO(Employee employee);
//
//  Employee empUpdateDTOToEmp(EmployeeUpdateDTO employeeUpdateDTO);
}
