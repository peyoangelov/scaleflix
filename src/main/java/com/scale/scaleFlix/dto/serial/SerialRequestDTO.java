package com.scale.scaleFlix.dto.serial;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
public class SerialRequestDTO {

    private long id;
    private String title;
    private String description;
    private double rating;
    private Date releaseDate;
    private String director;
    private String writer;
    private double stars;
    private double duration;
    private String imdbId;
    private String year;
    private String genre;
    private String audio;
    private String subtitles;
    private Short episodeNumber;
    private Short seasonNumber;
}
