package com.scale.scaleFlix.dto.serial;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
public class SerialResponseDTO {

    String title;
    String description;
    double rating;
    Date   releaseDate;
    String director;
    String writer;
    double stars;
    double duration;
    String imdbId;
    String year;
    String genre;
    String audio;
    String subtitles;
    Short  episodeNumber;
    Short  seasonNumber;
}
