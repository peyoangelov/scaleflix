package com.scale.scaleFlix.dto.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Getter
@Setter
@AllArgsConstructor
public class UserRequestDTO {
    @NotEmpty
    @Pattern(regexp = "[a-zA-Z0-9]*", message = "Password can have only letters and digits")
    @Size(min = 5, max = 25, message = "Username should be  between 5 and 25 characters")
    private String username;

    @NotEmpty
    @Pattern(regexp = "[a-zA-Z0-9]*", message = "Password can have only letters and digits")
    @Size(min = 5, max = 25, message = "Password should be  between 5 and 25 characters")
    private String password;

    @Email
    @NotEmpty
    private String email;

}
