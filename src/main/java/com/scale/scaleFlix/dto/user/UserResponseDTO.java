package com.scale.scaleFlix.dto.user;

import com.scale.scaleFlix.domain.Movie;
import com.scale.scaleFlix.domain.Serial;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
public class UserResponseDTO {
    @NotNull
    private Long id;
    @NotBlank
    private String username;
    @NotBlank
    private String email;

    private boolean isManager;

    private boolean isAdmin;

    private boolean isRegularUser;

    private Set<Serial> serials;

    private Set<Movie> movies;
}
