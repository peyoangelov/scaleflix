package com.scale.scaleFlix.dto.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class UserRoleUpdateDTO {
    private String email;
    private boolean isManager;

    private boolean isAdmin;

    private boolean isRegularUser;
}
