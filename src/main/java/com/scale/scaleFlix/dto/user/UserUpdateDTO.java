package com.scale.scaleFlix.dto.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class UserUpdateDTO {

    private String username;

    private String password;

    private String email;


}
