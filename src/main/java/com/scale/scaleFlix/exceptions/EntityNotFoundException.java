package com.scale.scaleFlix.exceptions;
public class EntityNotFoundException extends RuntimeException {
    public static final String USER_NOT_FOUND = "There is no entity with id: ";

    public EntityNotFoundException(Long id) {
        super(USER_NOT_FOUND + id);
    }
}
