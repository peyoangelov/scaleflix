package com.scale.scaleFlix.exceptions;

public class ExistingEmailException extends RuntimeException {
    public static final String EXISTING_EMAIL = "This email already exists.";

    public ExistingEmailException() {
        super(EXISTING_EMAIL);
    }
}

