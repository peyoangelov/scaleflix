package com.scale.scaleFlix.exceptions;

public class InvalidEmailException extends RuntimeException {
    public static final String INVALID_EMAIL = "Provided email is invalid.";

    public InvalidEmailException() {
        super(INVALID_EMAIL);
    }

}
