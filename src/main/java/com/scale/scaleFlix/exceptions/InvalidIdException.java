package com.scale.scaleFlix.exceptions;

public class InvalidIdException extends RuntimeException {
    public static final String INVALID_ID = "Provided id is invalid: ";

    public InvalidIdException() {
        super(INVALID_ID);
    }

    public InvalidIdException(Long id) {
        super(INVALID_ID + id);
    }
}
