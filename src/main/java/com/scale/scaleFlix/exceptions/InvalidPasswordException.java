package com.scale.scaleFlix.exceptions;

public class InvalidPasswordException extends RuntimeException {
    public static final String INVALID_PASSWORD = "Provided password is invalid";

    public InvalidPasswordException() {
        super(INVALID_PASSWORD);
    }
}
