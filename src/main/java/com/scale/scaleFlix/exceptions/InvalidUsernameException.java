package com.scale.scaleFlix.exceptions;

public class InvalidUsernameException extends  RuntimeException{
    public static final String EMPLOYEE_NOT_FOUND = "There is no user with username: ";

    public InvalidUsernameException(String username) {
        super(EMPLOYEE_NOT_FOUND + username);
    }
}
