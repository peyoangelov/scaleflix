package com.scale.scaleFlix.exceptions;

public class PermissionDeniedException extends RuntimeException{
    public static final String NO_PERMISSION = "You don`t have permission to do this action";
    public static final String SEARCH_DENIED_PERMISSION = " is not your name. You don`t have permission to do this action";

    public PermissionDeniedException(){
        super(NO_PERMISSION);
    }

    public PermissionDeniedException(String firstName){
        super(firstName + SEARCH_DENIED_PERMISSION);
    }

}
