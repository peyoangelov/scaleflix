package com.scale.scaleFlix.principal;

import lombok.Data;
import lombok.NonNull;

import javax.security.auth.Subject;
import java.security.Principal;

@Data
public class UserPrincipal implements Principal {
    @NonNull
    private long id;
    @NonNull
    private String email;
    @NonNull
    private boolean isManager;
    @NonNull
    private boolean isAdmin;
    @NonNull
    private boolean isRegularUser;

    @Override
    public boolean implies(Subject subject) {
        return Principal.super.implies(subject);
    }

    @Override
    public String getName() {
        return email;
    }
}
