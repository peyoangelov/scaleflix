package com.scale.scaleFlix.services;

import com.scale.scaleFlix.dto.user.LoginRequestDTO;

public interface AuthenticationService {
    String login(LoginRequestDTO loginRequestDTO);
}
