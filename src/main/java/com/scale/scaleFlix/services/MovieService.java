package com.scale.scaleFlix.services;

import com.scale.scaleFlix.dto.movie.MovieRequestDTO;
import com.scale.scaleFlix.dto.movie.MovieResponseDTO;
import org.springframework.data.crossstore.ChangeSetPersister;

import java.util.List;

public interface MovieService {
    List<MovieResponseDTO> getAllMovies();

    MovieResponseDTO getMovieById(long id) throws ChangeSetPersister.NotFoundException;

    MovieResponseDTO getMovieByTitle(String name) throws ChangeSetPersister.NotFoundException;

    void importMovieToDb(MovieRequestDTO model) throws Exception;

    void deleteMovieById(Long id);

    void deleteMovieByTitle(String title);

    List<MovieResponseDTO> getMoviesByGenre(String genre);

    List<MovieResponseDTO> getUserFavoritesMovies(Long id);

}
