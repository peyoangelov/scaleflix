package com.scale.scaleFlix.services;

import com.scale.scaleFlix.dto.serial.SerialRequestDTO;
import com.scale.scaleFlix.dto.serial.SerialResponseDTO;
import org.springframework.data.crossstore.ChangeSetPersister;

import java.util.List;

public interface SerialService {
    List<SerialResponseDTO> getAllSeries();

    SerialResponseDTO getSerialById(long id) throws ChangeSetPersister.NotFoundException;

    SerialResponseDTO getSerialByTitle(String title) throws ChangeSetPersister.NotFoundException;

    List<SerialResponseDTO> getSerialsByGenre(String genre);

    void importSerialToDb(SerialRequestDTO model) throws Exception;

    void deleteSerialById(Long id);

    void deleteSerialByTitle(String title);

    List<SerialResponseDTO> getUserFavoritesSerials(Long userId);
}
