package com.scale.scaleFlix.services;

import com.scale.scaleFlix.domain.User;
import com.scale.scaleFlix.principal.UserPrincipal;

public interface TokenService {
    String generateToken(User user);

    UserPrincipal parseToken(String token);
}
