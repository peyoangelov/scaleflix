package com.scale.scaleFlix.services;

import com.scale.scaleFlix.dto.user.UserRequestDTO;
import com.scale.scaleFlix.dto.user.UserResponseDTO;
import com.scale.scaleFlix.dto.user.UserRoleUpdateDTO;
import com.scale.scaleFlix.dto.user.UserUpdateDTO;
import com.scale.scaleFlix.principal.UserPrincipal;

import java.util.List;

public interface UserService {
    UserResponseDTO getUserById(Long id);

    UserResponseDTO getUserByEmail(String email);

    List<UserResponseDTO> findAll();

    void save(UserRequestDTO requestDTO);

    UserResponseDTO update(Long id, UserUpdateDTO userToUpdate);

    UserResponseDTO updateUserRole(Long id, UserRoleUpdateDTO userRoleUpdateDTO);

    String deleteUserById(Long id);

    UserResponseDTO getUserByUsername(String username);

    void addMovieToFavoriteList(Long userId, Long movieId);

    void addSerialToFavoriteList(Long userId, Long serialId);

    List<UserResponseDTO> findUserInfoByUsername(UserPrincipal principal, String username);

//    List<UserServiceModel> getUserUserServiceModelByRole(String role);
//
}