package com.scale.scaleFlix.services.impl;

import com.scale.scaleFlix.domain.Movie;
import com.scale.scaleFlix.domain.repositories.MovieRepository;
import com.scale.scaleFlix.dto.mapper.MovieMapper;
import com.scale.scaleFlix.dto.mapper.UserMapper;
import com.scale.scaleFlix.dto.movie.MovieRequestDTO;
import com.scale.scaleFlix.dto.movie.MovieResponseDTO;
import com.scale.scaleFlix.dto.user.UserResponseDTO;
import com.scale.scaleFlix.services.MovieService;
import com.scale.scaleFlix.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@ComponentScan
public class MovieServiceImpl implements MovieService {

    private final MovieMapper mapper = MovieMapper.INSTANCE;
    private final MovieRepository movieRepository;
    private final UserMapper userMapper = UserMapper.INSTANCE;
    private final UserService userService;

    @Override
    public List<MovieResponseDTO> getAllMovies() {

        return movieRepository.findAll()
                .stream()
                .map(movie -> mapper.movieToMovieResponseDTO(movie))
                .collect(Collectors.toList());
    }

    @Override
    public MovieResponseDTO getMovieById(long id) {
        Movie movie = movieRepository.getById(id);
        return mapper.movieToMovieResponseDTO(movie);

    }

    @Override
    public MovieResponseDTO getMovieByTitle(String title) {
        Movie movie = movieRepository.findByTitle(title);
        return mapper.movieToMovieResponseDTO(movie);
    }

    @Override
    public void importMovieToDb(MovieRequestDTO model) throws Exception {
        if (movieRepository.existsByImdbId(model.getImdbId())) {
            throw new Exception("The movie already exists!");
        }
        Movie movie = mapper.movieRequestDTOToMovie(model);
        movieRepository.save(movie);
    }

    @Override
    public void deleteMovieById(Long id) {
        movieRepository.deleteById(id);
    }

    @Override
    public void deleteMovieByTitle(String title) {
        movieRepository.deleteByTitle(title);
    }

    @Override
    public List<MovieResponseDTO> getMoviesByGenre(String genre) {
        return movieRepository.findByGenre(genre)
                .stream()
                .map(movie -> mapper.movieToMovieResponseDTO(movie))
                .collect(Collectors.toList());
    }

    @Override
    public List<MovieResponseDTO> getUserFavoritesMovies(Long userId) {
        UserResponseDTO user = userService.getUserById(userId);
        return user.getMovies()
                .stream()
                .map(movie -> mapper.movieToMovieResponseDTO(movie))
                .collect(Collectors.toList());

    }




}
