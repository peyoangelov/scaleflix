package com.scale.scaleFlix.services.impl;

import com.scale.scaleFlix.domain.Serial;
import com.scale.scaleFlix.domain.repositories.SerialRepository;
import com.scale.scaleFlix.dto.mapper.SerialMapper;
import com.scale.scaleFlix.dto.serial.SerialRequestDTO;
import com.scale.scaleFlix.dto.serial.SerialResponseDTO;
import com.scale.scaleFlix.dto.user.UserResponseDTO;
import com.scale.scaleFlix.services.SerialService;
import com.scale.scaleFlix.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@ComponentScan
public class SerialServiceImpl implements SerialService {

    private final SerialMapper mapper = SerialMapper.INSTANCE;
    private final SerialRepository serialRepository;
    private final UserService userService;

    @Override
    public List<SerialResponseDTO> getAllSeries() {
        return serialRepository.findAll()
                .stream()
                .map(serial -> mapper.serialToSerialResponseDTO(serial))
                .collect(Collectors.toList());
    }

    @Override
    public SerialResponseDTO getSerialById(long id) {
        Serial serial = serialRepository.getById(id);
        return mapper.serialToSerialResponseDTO(serial);
    }

    @Override
    public SerialResponseDTO getSerialByTitle(String title) {
        Serial serial = serialRepository.findByTitle(title);
        return mapper.serialToSerialResponseDTO(serial);
    }

    @Override
    public List<SerialResponseDTO> getSerialsByGenre(String genre) {

        return serialRepository.findByGenre(genre)
                .stream()
                .map(movie -> mapper.serialToSerialResponseDTO(movie))
                .collect(Collectors.toList());
    }

    @Override
    public void importSerialToDb(SerialRequestDTO model) throws Exception {
        if (serialRepository.existsByImdbId(model.getImdbId())) {
            throw new Exception("The serial already exists!");
        }
        Serial serial = mapper.serialRequestDTOToSerial(model);
        serialRepository.save(serial);

    }

    @Override
    public void deleteSerialById(Long id) {
        serialRepository.deleteById(id);
    }

    @Override
    public void deleteSerialByTitle(String title) {
        serialRepository.deleteByTitle(title);
    }

    @Override
    public List<SerialResponseDTO> getUserFavoritesSerials(Long userId) {
        UserResponseDTO user = userService.getUserById(userId);
        return user.getSerials()
                .stream()
                .map(serial -> mapper.serialToSerialResponseDTO(serial))
                .collect(Collectors.toList());
    }

}
