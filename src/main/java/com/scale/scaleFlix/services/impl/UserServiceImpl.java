package com.scale.scaleFlix.services.impl;

import com.scale.scaleFlix.domain.Movie;
import com.scale.scaleFlix.domain.Serial;
import com.scale.scaleFlix.domain.User;
import com.scale.scaleFlix.domain.repositories.MovieRepository;
import com.scale.scaleFlix.domain.repositories.SerialRepository;
import com.scale.scaleFlix.domain.repositories.UserRepository;
import com.scale.scaleFlix.dto.mapper.CycleAvoidingMappingContext;
import com.scale.scaleFlix.dto.mapper.MovieMapper;
import com.scale.scaleFlix.dto.mapper.UserMapper;
import com.scale.scaleFlix.dto.user.UserRequestDTO;
import com.scale.scaleFlix.dto.user.UserResponseDTO;
import com.scale.scaleFlix.dto.user.UserRoleUpdateDTO;
import com.scale.scaleFlix.dto.user.UserUpdateDTO;
import com.scale.scaleFlix.exceptions.*;
import com.scale.scaleFlix.principal.UserPrincipal;
import com.scale.scaleFlix.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
@RequiredArgsConstructor
@ComponentScan
public class UserServiceImpl implements UserService {

    private final CycleAvoidingMappingContext context;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final UserMapper userMapper = UserMapper.INSTANCE;
    private final MovieRepository movieRepository;
    private final SerialRepository serialRepository;
    private final MovieMapper movieMapper = MovieMapper.INSTANCE;


    @Override
    public UserResponseDTO getUserById(Long id) {

        User user = findById(id);
        return userMapper.userToUserRespDTO(user);
    }

    private User findById(Long id) {

        return userRepository.findById(id)
                .orElseThrow(() -> new InvalidIdException(id));
    }

    private Movie findMovieById(Long id) {

        return movieRepository.findById(id)
                .orElseThrow(() -> new InvalidIdException(id));
    }

    @Override
    public UserResponseDTO getUserByUsername(String username) {

        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new InvalidUsernameException(username));

        return userMapper.userToUserRespDTO(user);
    }

    @Override
    public void addMovieToFavoriteList(Long userId, Long movieId) {

        Movie movie = movieRepository.getById(movieId);
        User user = userRepository.getById(userId);

        Set<Movie> favoritesMovies = user.getMovies();
        favoritesMovies.add(movie);

        userRepository.save(user);
        movieRepository.save(movie);

    }

    @Override
    public void addSerialToFavoriteList(Long userId, Long serialId) {

        Serial serial = serialRepository.getById(serialId);
        User user = userRepository.getById(userId);

        Set<Serial> favoritesSerials = user.getSerials();
        favoritesSerials.add(serial);
        userRepository.save(user);
        serialRepository.save(serial);
    }

    @Override
    public List<UserResponseDTO> findUserInfoByUsername(UserPrincipal principal, String username) {

        List<User> allUsers = userRepository.findAll();
        return getNameResponseDTOList(principal, username, allUsers);

    }

    private List<UserResponseDTO> getNameResponseDTOList(UserPrincipal principal,
                                                         String userName,
                                                         List<User> users) {

        Optional<UserResponseDTO> singleUser = users.stream()
                .filter(user -> user.getEmail().equals(principal.getEmail()))
                .filter(user -> user.getUsername().equalsIgnoreCase(userName))
                .map(userMapper::userToUserRespDTO)
                .findFirst();
        if (singleUser.isPresent()) {
            return List.of(singleUser.get());
        } else {
            throw new PermissionDeniedException(userName);
        }
    }

    @Override
    public UserResponseDTO getUserByEmail(String email) {

        User user = userRepository.findByEmail(email)
                .orElseThrow(UserNotFoundException::new);

        return userMapper.userToUserRespDTO(user);
    }

    @Override
    public List<UserResponseDTO> findAll() {

        List<UserResponseDTO> userBindingModelList = new ArrayList<>();
        userRepository.findAll().forEach(user -> userBindingModelList.add(userMapper.userToUserRespDTO(user)));

        return userBindingModelList;

    }

    @Override
    public void save(UserRequestDTO requestDTO) {

        if (userRepository.findByEmail(requestDTO.getEmail()).isPresent()) {
            throw new ExistingEmailException();
        }
        User user = userMapper.userReqDTOToUser(requestDTO);
        user.setPassword(passwordEncoder.encode(user.getPassword()));

        user.setRegularUser(true);
        userRepository.save(user);

        userMapper.userToUserRespDTO(user);
    }

    @Override
    public UserResponseDTO update(Long id, UserUpdateDTO userToUpdate) {

        User tempUser = findById(id);
        if (!tempUser.getEmail().equals(userToUpdate.getEmail())) {
            boolean exists = userRepository.findByEmail(userToUpdate.getEmail()).isPresent();
            if (exists) {
                throw new ExistingEmailException();
            }
            tempUser.setEmail(userToUpdate.getEmail());
        }

        tempUser.setUsername(userToUpdate.getUsername());
        tempUser.setPassword(passwordEncoder.encode(userToUpdate.getPassword()));

        userRepository.save(tempUser);
        return userMapper.userToUserRespDTO(tempUser);
    }

    @Override
    public UserResponseDTO updateUserRole(Long id, UserRoleUpdateDTO userRoleUpdateDTO) {

        User tempUser = findById(id);
        if (!tempUser.getEmail().equals(userRoleUpdateDTO.getEmail())) {
            boolean exists = userRepository.findByEmail(userRoleUpdateDTO.getEmail()).isPresent();
            if (exists) {
                throw new ExistingEmailException();
            }
        }
        tempUser.setManager(userRoleUpdateDTO.isManager());
        tempUser.setAdmin(userRoleUpdateDTO.isAdmin());

        userRepository.save(tempUser);
        return userMapper.userToUserRespDTO(tempUser);
    }

    @Override
    public String deleteUserById(Long id) {

        userRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(id));

        userRepository.deleteById(id);
        return "The Entity has been deleted successfully!";
    }


}
