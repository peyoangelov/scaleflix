package com.scale.scaleFlix.services.impl.auth;

import com.scale.scaleFlix.domain.User;
import com.scale.scaleFlix.domain.repositories.UserRepository;
import com.scale.scaleFlix.dto.user.LoginRequestDTO;
import com.scale.scaleFlix.exceptions.InvalidPasswordException;
import com.scale.scaleFlix.services.AuthenticationService;
import com.scale.scaleFlix.services.TokenService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthenticationServiceImpl implements AuthenticationService {
    private final PasswordEncoder passwordEncoder;
    private final TokenService tokenService;
    private final UserRepository userRepository;

    @Override
    public String login(LoginRequestDTO loginRequestDTO) {
        User user = userRepository.findByEmail(loginRequestDTO.getEmail())
                .orElseThrow(InvalidPasswordException::new);
        if (!passwordEncoder.matches(loginRequestDTO.getPassword(), user.getPassword())) {
            throw new InvalidPasswordException();
        }
        return tokenService.generateToken(user);
    }
}
