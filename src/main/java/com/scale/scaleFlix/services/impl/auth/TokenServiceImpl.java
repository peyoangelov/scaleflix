package com.scale.scaleFlix.services.impl.auth;

import com.scale.scaleFlix.domain.User;
import com.scale.scaleFlix.principal.UserPrincipal;
import com.scale.scaleFlix.services.TokenService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;

@Service
public class TokenServiceImpl implements TokenService {
    private final String JWT_SECRET = "mvsPwatvFiep1Un1MOgpUEoLbJjW8xAN";
    public static final String BEARER_ID_FIELD = "id";
    public static final String BEARER_SUBJECT_FIELD = "sub";
    public static final String BEARER_ADMIN_FIELD = "ADMIN";
    public static final String BEARER_MANAGER_FIELD = "MANAGER";
    public static final String BEARER_USER_FIELD = "USER";

    @Override
    public String generateToken(User user) {
        Instant expirationTime = Instant.now().plus(1, ChronoUnit.DAYS);
        Date expirationDate = Date.from(expirationTime);

        Key key = Keys.hmacShaKeyFor(JWT_SECRET.getBytes(StandardCharsets.UTF_8));

        String compactTokenString = Jwts.builder()
                .claim(BEARER_ID_FIELD, user.getId())
                .claim(BEARER_SUBJECT_FIELD, user.getEmail())
                .claim(BEARER_MANAGER_FIELD, user.isManager())
                .claim(BEARER_ADMIN_FIELD, user.isAdmin())
                .claim(BEARER_USER_FIELD, user.isRegularUser())
                .setExpiration(expirationDate)
                .signWith(key, SignatureAlgorithm.HS256)
                .compact();

        return "Bearer " + compactTokenString;
    }

    @Override
    public UserPrincipal parseToken(String token) {
        byte[] secretBytes = JWT_SECRET.getBytes(StandardCharsets.UTF_8);
        Jws<Claims> jwsClaims = Jwts.parserBuilder()
                .setSigningKey(secretBytes)
                .build()
                .parseClaimsJws(token);

        Long id = jwsClaims.getBody().get(BEARER_ID_FIELD, Long.class);
        String email = jwsClaims.getBody().getSubject();
        boolean isManager = jwsClaims.getBody().get(BEARER_MANAGER_FIELD, Boolean.class);
        boolean isAdmin = jwsClaims.getBody().get(BEARER_ADMIN_FIELD, Boolean.class);
        boolean isUser = jwsClaims.getBody().get(BEARER_USER_FIELD, Boolean.class);

        return new UserPrincipal(id, email, isManager, isAdmin, isUser);
    }
}
