package com.scale.scaleFlix.services.validation.base;

import com.scale.scaleFlix.domain.repositories.UserRepository;
import org.springframework.stereotype.Service;

@Service
public class AuthValidationServiceImpl {
    private final UserRepository userRepository;

    public AuthValidationServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

//    @Override
//    public boolean isValid(RegisterUserServiceModel user) {
//        return this.isEmailValid(user.getEmail()) &&
//                this.arePasswordsValid(user.getPassword(), user.getConfirmPassword()) &&
//                this.isUsernameFree(user.getUsername());
//    }

    private boolean arePasswordsValid(String password, String confirmPassword) {
        return password.equals(confirmPassword);
    }

    private boolean isUsernameFree(String username) {
        return !userRepository.existsByUsername(username);
    }

    private boolean isEmailValid(String email) {
        return true;
    }
}
