package com.scale.scaleFlix.utils;

import com.scale.scaleFlix.domain.User;
import com.scale.scaleFlix.domain.repositories.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.SmartInitializingSingleton;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@ComponentScan
@AllArgsConstructor
public class RootUserInitializer implements SmartInitializingSingleton {

    private PasswordEncoder passwordEncoder;
    private UserRepository userRepository;

    @Override
    public void afterSingletonsInstantiated() {
        Optional<User> user = userRepository.findByEmail("admin@gmail.com");
        if (user.isEmpty()) {
            createRootUser();
        }
    }

    private void createRootUser() {
        User ceo = new User("admin","admin","admin@gmail.com");
        ceo.setPassword(passwordEncoder.encode(ceo.getPassword()));
        ceo.setRegularUser(false);
        ceo.setAdmin(true);
        ceo.setManager(false);

        userRepository.save(ceo);
    }
}